import * as React from 'react';
import { render } from '@testing-library/react';
import { expect } from 'chai';
import App from './pages/Count';

describe('<App>', () => {
  it('renders learn react link', () => {
    const { getByText } = render(<App />);
    const linkElement = getByText(/learn react/i);
    expect(document.body.contains(linkElement));
  });
});
