import { atom } from 'recoil';

interface IpersonList {
  name: string;
  familyName: string;
}

export const personList = atom<Array<IpersonList>>({
  key: 'personList',
  default: [{ name: '小明', familyName: '陈' }],
});
