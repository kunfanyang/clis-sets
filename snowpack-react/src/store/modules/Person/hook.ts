import { useRecoilState } from 'recoil';

import { personList } from './store';

interface IAddItem {
  name: string;
  familyName: string;
}

const addPersonList = (item: IAddItem) => {
  const [personListTotal, usePersonList] = useRecoilState(personList);
  return () => {
    usePersonList([...personListTotal, item]);
  };
};

export default { addPersonList, personList };
