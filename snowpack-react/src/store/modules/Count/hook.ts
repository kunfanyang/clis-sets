import { count, computedIsOdd } from './store';

import { useRecoilState } from 'recoil';

// Action层

/**
 * 修改数据
 */
const updateCount = () => {
  const [counter, useCount] = useRecoilState(count);

  // recoil相关操作只能通过闭包将函数暴露给react内的Function
  return () => {
    const newCounter = counter + 111;
    useCount(newCounter);
  };
};

export { count, computedIsOdd, updateCount };
