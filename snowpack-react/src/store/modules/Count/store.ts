import { atom, selector } from 'recoil';

export const count = atom({
  key: 'count',
  default: 123,
});

export const computedIsOdd = selector({
  key: 'computeOdd',
  get: ({ get }) => {
    const num = get(count);

    return num % 2 === 0;
  },
});
