import React, { useReducer, useCallback, useMemo } from 'react';

interface Istate {
    value: number
}
interface Iaction {
    type: string
}

const initialState = {
    value: 1
}

function ListReducer (state: Istate, action: Iaction): Istate {
    if (action.type === 'add') {
        return {
            value: state.value + 1
        }
    }

    return state
}

function TestHook () {
    const [state, dispatch] = useReducer(ListReducer, initialState)

    const addCount = useCallback(() => {
        console.log('state :>> ', state);
        dispatch({type: 'add'})
    }, [state])

    const calcState = useMemo(() => {
        return state.value * 10000
    }, [state])

    return <>
        <p onClick={() => addCount()} style={{textAlign: 'center'}}>
            {state.value}{calcState}
        </p>
    </>
}

export default TestHook