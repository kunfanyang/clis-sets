import React, {useEffect, useState} from "react"

export default function Demo () {
    let [status, useStatus] = useState<Boolean>(true)

    useEffect(() => {
        document.title = status ? 'ON' : 'OFF'

        return  () =>  {
            console.log(888)
        }
    }, [status])

    function toggleStatus () {
        useStatus(!status)
    }

    return <>
        <p onClick={() => toggleStatus()}>{status ? 'ON' : 'OFF'}</p>
    </>
}