import React, {useEffect} from 'react';
import { count, computedIsOdd} from '../store/modules/Count/hook'
import { useRecoilValue } from "recoil";

export default function AppTop () {
  const counter = useRecoilValue(count)
  const receiveComputedIsOdd = useRecoilValue(computedIsOdd)

  useEffect(() => {
    console.log(receiveComputedIsOdd)
  }, [counter])

  return <>
    <p>Another Page Count:<code>{counter}</code></p>
    <p>{receiveComputedIsOdd ? 'isOdd' : 'noOdd'}</p>
  </>
}