import React from 'react';
import logo from '../logo.svg';
import './Count.css';
import SubCount from "./SubCount"

import {updateCount, count} from '../store/modules/Count/hook'
import { useRecoilValue } from "recoil";

function App() {
  const counter = useRecoilValue(count)
  const updateCounter = updateCount()

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p onClick={() => updateCounter()}>点击更新</p>
        <p>
          App Page Count:<code>{counter}</code>
        </p>
        <SubCount/>
      </header>
    </div>
  );
}

export default App;
