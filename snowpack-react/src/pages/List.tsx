import React from 'react'
import './List.css'
import TotalPersonList from '../store/modules/Person/hook'
import {useRecoilValue} from "recoil"

function ListApp () {
    const personList = useRecoilValue(TotalPersonList.personList)

    const addPersonList = TotalPersonList.addPersonList({name: '凯', familyName: '阮'})

    return <div className='list'>
        <button onClick={() => addPersonList()}>添加</button>
        <ul>
            {
              personList.map((item, index) => <li key={index}>{item.familyName}--{item.name}</li>)
            }
        </ul>
    </div>
}

export default ListApp
