import React, { useEffect } from 'react';

import { of, pipe } from 'rxjs';
import { filter, map } from 'rxjs/operators';

function discardOddDoubleEven() {
    return pipe(
      filter(v => ! (v % 2)),
      map(v => v + v),
    );
  }

function TestHookChild () {
    useEffect(() => {
        discardOddDoubleEven()(of(1, 2, 3)).subscribe((v) => {
            console.log(`value is ${v}`);
        })
    }, [])
    return <>
        测试rxjs
    </>
}

export default TestHookChild