import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';

const Count = React.lazy(() => import('./pages/Count'))
const Demo = React.lazy(() => import('./pages/Demo'))
const List = React.lazy(() => import('./pages/List'))
const TestHook = React.lazy(() => import('./pages/TestHook'))
const TestRXJS = React.lazy(() => import('./pages/TestRXJS'))

import './index.css';

import {RecoilRoot} from 'recoil';

import {
  BrowserRouter as Router, Switch, Route
} from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <RecoilRoot>
      <Suspense fallback={<div>加载中。。。</div>}>
        <Router>
          <Switch>
            <Route path="/count"><Count/></Route>

            <Route path="/list"><List/></Route>

            <Route path="/demo"><Demo/></Route>

            <Route path="/test-hook"><TestHook/></Route>

            <Route path="/test-rxjs"><TestRXJS/></Route>
          </Switch>
        </Router>
      </Suspense>
    </RecoilRoot>
  </React.StrictMode>,
  document.getElementById('root'),
);

// Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
// Learn more: https://www.snowpack.dev/#hot-module-replacement
if (import.meta.hot) {
  import.meta.hot.accept();
}
